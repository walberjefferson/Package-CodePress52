<?php

namespace CodePress\CodeDatabase\Tests;

use CodePress\CodeDatabase\Contracts\CriteriaInterface;
use CodePress\CodeDatabase\Tests\Criteria\FindByNameAndDescription;
use CodePress\CodeDatabase\Tests\Model\Category;
use CodePress\CodeDatabase\Tests\Repository\CategoryRepository;
use Illuminate\Database\Query\Builder;
use Mockery as m;

class FindByNameAndDescriptionTest extends AbstractTestCase
{
    /**
     * @var \CodePress\CodeDatabase\Tests\Repository\CategoryRepository
     */
    private $repository;

    /**
     * @var \CodePress\CodeDatabase\Tests\Criteria\FindByNameAndDescription
     */
    private $criteria;

    public function setUp()
    {
        parent::setUp();
        //$this->migrate();
        $this->repository = new CategoryRepository();
        $this->criteria = new FindByNameAndDescription();
        //$this->createCategory();
    }

    public function test_if_instanceof_criteriainterface()
    {
        $this->assertInstanceOf(CriteriaInterface::class, $this->criteria);
    }

    public function test_if_apply_returns_querybuilder()
    {
        $class = $this->repository->model();
        $result = $this->criteria->apply(new $class, $this->repository);
        $this->assertInstanceOf(Builder::class, $result);
    }

    private function createCategory()
    {
        Category::create(['name' => 'Category 1', 'description' => 'Description 1']);
        Category::create(['name' => 'Category 2', 'description' => 'Description 2']);
        Category::create(['name' => 'Category 3', 'description' => 'Description 3']);
    }
}