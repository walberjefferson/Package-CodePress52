<?php

namespace CodePress\CodeDatabase\Tests\Repository;

use CodePress\CodeDatabase\AbstractRepository;
use CodePress\CodeDatabase\Tests\Model\Category;

class CategoryRepository extends AbstractRepository
{
    public function model()
    {
        return Category::class;
    }

    


}