<?php

namespace CodePress\CodeDatabase\Tests\Criteria;

use CodePress\CodeDatabase\Contracts\CriteriaInterface;
use CodePress\CodeDatabase\Contracts\RepositoryInterface;

class FindByNameAndDescription implements CriteriaInterface
{

    public function apply($model, RepositoryInterface $repository)
    {
        $model->where('name');
    }
}