<?php

namespace CodePress\CodeTags\Models;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{

    protected $table = "codepress_tags";

    protected $fillable = [
        'name',
        'parent_id'
    ];

    public function taggable()
    {
        return $this->morphTo();
    }

    public function parent()
    {
        return $this->belongsTo(Tag::class);
    }

    public function children()
    {
        return $this->hasMany(Tag::class, 'parent_id');
    }
}